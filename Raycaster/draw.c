#include "draw.h"
#include "game.h"
#include "raycaster.h"
#include "texture.h"
#include <stdio.h>
#include <math.h>

static double viewAngleSine;

void clearScreen(SDL_Surface *screen) {
    SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
}

static int getTextureX(Map *map, PlayerState *player, Texture *texture, double x, double y) {
    double deltax;
    //We need to know what direction the wall is facing
    WallDirection dir = getWallDirection(map, player, x, y);
    switch (dir) {
    case WD_Up:
        deltax = MAP_GRID_DISTANCE - doubleMod(x, MAP_GRID_DISTANCE);
        break;
    case WD_Down:
        deltax = doubleMod(x, MAP_GRID_DISTANCE);
        break;
    case WD_Left:
        deltax = MAP_GRID_DISTANCE - doubleMod(y, MAP_GRID_DISTANCE);
        break;
    case WD_Right:
        deltax = doubleMod(y, MAP_GRID_DISTANCE);
        break;
    }
    return (int)(deltax*texture->width/MAP_GRID_DISTANCE);
}

static /*inline*/ int getTextureY(int y, int height, Texture *texture) {
    return y*texture->height/height;
}

//Does not check if the x and y coordinates are actually within the permitted range
static void drawPixel(int x, int y, int r, int g, int b, SDL_Surface *screen) {
    Uint32 *pixmem32;
    Uint32 color;

    color = SDL_MapRGB(screen->format, r, g, b);

    pixmem32 = (Uint32*) screen->pixels  + screen->pitch/4*y + x;
    *pixmem32 = color;
}

static void drawPixelColor(int x, int y, Uint32 color, SDL_Surface *screen) {
    Uint32 *pixmem32;

    if (x >= 0 && x < X_RESOLUTION && y >= 0 && y < Y_RESOLUTION) {
        pixmem32 = (Uint32*) screen->pixels  + screen->pitch/4*y + x;
        *pixmem32 = color;
    }
}

static /*inline*/ Uint32 getPixelColor(int x, int y, SDL_Surface *surface) {
    //if (x >= 0 && x < X_RESOLUTION && y >= 0 && y < Y_RESOLUTION) {
    return *((Uint32*)surface->pixels + surface->pitch/4*y + x);
    //}
    //return 0;
}

static void verticalTextureLine(Map *map, PlayerState *player, double x, double y, int screenx,
                                int distance, int height, Texture *texture, SDL_Surface *screen) {
    int linex = getTextureX(map, player, texture, x, y);
    int liney = 0;
    int currenty = 0;
    Uint32 color;
    Uint8 r, g, b;
    int starty = 0, endy = height;
    //printf("%d\n", distance);
    if (height >= Y_RESOLUTION) {
        starty = (height - Y_RESOLUTION + 1)/2;
        endy = height - starty;
    }
    for(currenty = starty; currenty < endy; currenty++) {
        liney = getTextureY(currenty, height, texture);
        color = getPixelColor(linex, liney, texture->texture);
        SDL_GetRGB(color, screen->format, &r, &g, &b);
        if (distance > MAP_GRID_DISTANCE) {
            r = SHADED_COLOR_VALUE(r, distance);
            g = SHADED_COLOR_VALUE(g, distance);
            b = SHADED_COLOR_VALUE(b, distance);
        }
        drawPixel(screenx, Y_RESOLUTION/2 - height/2 + currenty, r, g, b, screen);
    }
}

static void horizontalLine(int x1, int y, int x2, SDL_Surface *screen) {
    int i;
    if (x1 == x2) {
        drawPixel(x1, y, 0, 0xFF, 0, screen);
    }
    else if (x1 < x2) {
        for (i = x1; i <= x2; i++) {
            drawPixel(i, y, 0, 0xFF, 0, screen);
        }
    }
    else{
        for (i = x2; i <= x1; i++) {
            drawPixel(i, y, 0, 0xFF, 0, screen);
        }
    }
}

static void verticalLine(int x, int y1, int y2, SDL_Surface *screen) {
    int i;
    if (y1 == y2) {
        drawPixel(x, y1, 0, 0xFF, 0, screen);
    }
    else if (y1 < y2) {
        for (i = y1; i <= y2; i++) {
            drawPixel(x, i, 0, 0xFF, 0, screen);
        }
    }
    else{
        for (i = y2; i <= y1; i++) {
            drawPixel(x, i, 0, 0xFF, 0, screen);
        }
    }
}

//Used for grayscale walls
static void shadedVerticalLine(int x, int y1, int y2, int distance, SDL_Surface *screen) {
    int i;
    int c;
    if (distance == 0) {
        return;
    }
    c = 0xFF - distance;
    if (c < 0) {
        c = 0;
    }
    if (y1 == y2) {
        drawPixel(x, y1, c, c, c, screen);
    }
    else if (y1 < y2) {
        for (i = y1; i <= y2; i++) {
            drawPixel(x, i, c, c, c, screen);
        }
    }
    else{
        for (i = y2; i <= y1; i++) {
            drawPixel(x, i, c, c, c, screen);
        }
    }
}

//Simple line drawing function
static void drawLine(int x1, int y1, int x2, int y2, SDL_Surface *screen) {
    if (x1 == x2) {
        verticalLine(x1, y1, y2, screen);
    }
    else if (y1 == y2) {
        horizontalLine(x1, y1, x2, screen);
    }
    else {
        int x = x1;
        double y = (double)y1;
        int step = x1 < x2 ? 1 : -1;
        double slope = (double)(y2 - y1)/(double)(x2 - x1)*(double)step;
        while (x != x2) {
            verticalLine(x, (int)y, (int)(y + slope), screen);
            x += step;
            y += slope;
        }
    }
}

void drawMap2D(Map *map, SDL_Surface *screen) {
    int y, x;
    int color;
    for (y = 0; y < map->ySize; y++) {
        for (x = 0; x < map->xSize; x++) {
            SDL_Rect rect = {SQUARE_SIZE_2D*x, SQUARE_SIZE_2D*y, SQUARE_SIZE_2D, SQUARE_SIZE_2D};
            color = map->map[y][x]*0xFF;
            SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, color, color, color));
        }
    }
}

void drawPlayer2D(Map *map, PlayerState *player, SDL_Surface *screen) {
    drawPixel(DISPLAY_COORDINATE(player->x), DISPLAY_COORDINATE(player->y), 0xFF, 0, 0, screen);
}

void drawRay2D(Map *map, PlayerState *player, SDL_Surface *screen) {
    double x = 0;
    double y = 0;
    int i;
    for (i = -100*VIEW_ANGLE; i < 100*VIEW_ANGLE; i++) {
        RayCaster_castRay(map, player, player->direction + (double)i/100, &x, &y);
        drawLine(DISPLAY_COORDINATE(player->x), DISPLAY_COORDINATE(player->y), DISPLAY_COORDINATE(x), DISPLAY_COORDINATE(y), screen);
    }
}

static double getRayCasterAngle(double direction, int n) {
    return direction + asin(2*n*sin(VIEW_ANGLE)/X_RESOLUTION);
}

void drawMap3D(Map *map, PlayerState *player, Texture *texture, SDL_Surface *screen) {
    int x, n;
    double wallx = 0, wally = 0;
    double distance, direction;
    int height;
    for (x = 0; x < X_RESOLUTION; x++) {
        n = -X_RESOLUTION/2 + x;
        direction = getRayCasterAngle(player->direction, n);
        distance = RayCaster_castRay(map, player, direction, &wallx, &wally);
        height = WALL_HEIGHT(distance);
        //shadedVerticalLine(x, Y_RESOLUTION/2 - height, Y_RESOLUTION/2 + height, (int)distance, screen);
        verticalTextureLine(map, player, wallx, wally, x, (int)distance, height, texture, screen);
    }
}



