#ifndef GAME_H
#include "game.h"
#include <SDL/SDL.h>
#include <math.h>

void handleKeyboardInput(Map *map, PlayerState *player) {
    Uint8* keyState = SDL_GetKeyState(NULL);
    if (keyState[SDLK_UP]) {
        player->x+=2*cos(player->direction);
        player->y+=2*sin(player->direction);
    }
    if (keyState[SDLK_DOWN]) {
        player->x-=2*cos(player->direction);
        player->y-=2*sin(player->direction);
    }
    if (keyState[SDLK_LEFT]) {
        player->direction-=.05;
    }
    if (keyState[SDLK_RIGHT]) {
        player->direction+=.05;
    }
}
#endif
