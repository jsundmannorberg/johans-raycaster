#ifndef MAP_H

#define MAP_H

#include "texture.h"

#define MAP_GRID_DISTANCE 20//50 //40

#define MAP_COORDINATE(x) ((x)/MAP_GRID_DISTANCE)

typedef struct {
    int **map;
    int xSize;
    int ySize;
    int xStart;
    int yStart;
    Texture wallTexture;
} Map;

void createMap(Map **map, int **mapArray, int xSize, int ySize, int xStart, int yStart);
void destroyMap(Map *map);

#endif // MAP_H
