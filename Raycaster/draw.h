#ifndef DRAW_H

#define DRAW_H

#include <SDL/SDL.h>
#include "game.h"

#define X_RESOLUTION 800
#define Y_RESOLUTION 600

#define VIEW_ANGLE .45 //.52 //.45

#define SQUARE_SIZE_2D 10

#define WALL_HEIGHT_FACTOR 20

#define WALL_HEIGHT(x) (int)(WALL_HEIGHT_FACTOR*(double)Y_RESOLUTION/(x))

#define DISPLAY_COORDINATE(x) (SQUARE_SIZE_2D*(((int)x)/MAP_GRID_DISTANCE) + \
                              (((int)(x))%MAP_GRID_DISTANCE)*SQUARE_SIZE_2D/MAP_GRID_DISTANCE)

#define SHADED_COLOR_VALUE(color, distance) (MAP_GRID_DISTANCE*(color)/(distance))


void clearScreen(SDL_Surface *screen);
void drawMap2D(Map *map, SDL_Surface *screen);
void drawPlayer2D(Map *map, PlayerState *player, SDL_Surface *screen);
void drawMap3D(Map *map, PlayerState *player, Texture *texture, SDL_Surface *screen);
void drawRay2D(Map *map, PlayerState *player, SDL_Surface *screen);

#endif
