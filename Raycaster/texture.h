#ifndef TEXTURE_H

#define TEXTURE_H
#include <SDL/SDL.h>

typedef struct {
    SDL_Surface *texture;
    int width;
    int height;
} Texture;

void loadTexture(char *fileName, Texture *texture);

#endif // TEXTURE_H
