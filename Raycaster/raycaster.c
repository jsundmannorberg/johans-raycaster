#include "game.h"
#include "raycaster.h"
#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#define ON_MAP_GRID(x) (doubleMod((x), MAP_GRID_DISTANCE) == 0)

inline double doubleMod(double x, int y) {
    return x - y*((int)x/y);
}

static void getNextxy(double x, double y, double deltax, double deltay, double *nextx, double *nexty) {
    double xToGrid = 0;
    double yToGrid = 0;
    double xx, xy, yx, yy;

    //Calculate distances to the next grid line intersections
    if (deltax < 0) {
        xToGrid = -doubleMod(x, MAP_GRID_DISTANCE);
    }
    else if (deltax > 0) {
        xToGrid = MAP_GRID_DISTANCE - doubleMod(x, MAP_GRID_DISTANCE);
    }
    if (deltay < 0) {
        yToGrid = -doubleMod(y, MAP_GRID_DISTANCE);
    }
    else if (deltay > 0) {
        yToGrid = MAP_GRID_DISTANCE - doubleMod(y, MAP_GRID_DISTANCE);
    }
    //If the distance to the nearest intersection is 0, use the next one instead
    if (xToGrid == 0 && deltax != 0) {
        xToGrid = deltax < 0 ? -MAP_GRID_DISTANCE : MAP_GRID_DISTANCE;
    }
    if (yToGrid == 0 && deltay != 0) {
        yToGrid = deltay < 0 ? -MAP_GRID_DISTANCE : MAP_GRID_DISTANCE;
    }
    //for vertical rays
    if (deltax == 0) {
        *nextx = x;
        *nexty = y + yToGrid;
        return;
    }
    //for horizontal rays
    else if (deltay == 0) {
        *nextx = x + xToGrid;
        *nexty = y;
        return;
    }

    xx = xToGrid;
    xy = xToGrid*deltay/deltax;
    yy = yToGrid;
    yx = yToGrid*deltax/deltay;

    //Compare the square of the distance to the x and y axes
    if (xx*xx + xy*xy > yy*yy + yx*yx) {
        *nextx = x + yx;
        *nexty = y + yy;
    }
    else {
        *nextx = x + xx;
        *nexty = y + xy;
    }
}

//Casts a ray and returns the collision coordinates
double RayCaster_castRay(Map *map, PlayerState *player, double direction, double *x, double *y) {
    double deltax = cos(direction);
    double deltay = sin(direction);
    double currentx = player->x;
    double currenty = player->y;
    //TODO: This approach is ugly, fix it!!!
    while (map->map[MAP_COORDINATE((int)(currenty + .10*deltay))][MAP_COORDINATE((int)(currentx + .10*deltax))] == 0) {
        getNextxy(currentx, currenty, deltax, deltay, &currentx, &currenty);
    }
    *x = currentx;
    *y = currenty;

    return sqrt((player->x-currentx)*(player->x-currentx)+(player->y-currenty)*(player->y-currenty))*cos(player->direction - direction);
}

WallDirection getWallDirection(Map *map, PlayerState *player, double x, double y) {
    //Are we on the vertical grid?
    if (ON_MAP_GRID(x)) {
        //It's either right or left
        //Is the player to the right of the wall?
        if (player->x > x) {
            return WD_Right;
        }
        else {
            return WD_Left;
        }
    }
    else {
        //up or down
        //Is the player below the wall?
        if (player->y > y) {
            return WD_Up;
        }
        else {
            return WD_Down;
        }
    }
}
