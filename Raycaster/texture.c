#include "texture.h"

void loadTexture(char *fileName, Texture *texture) {
    SDL_Surface *t = SDL_LoadBMP(fileName);
    texture->texture = SDL_DisplayFormat(t);
    texture->width = texture->texture->w;
    texture->height = texture->texture->h;
}


