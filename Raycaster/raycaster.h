#ifndef RAYCASTER_H
#define RAYCASTER_H

typedef enum {
    WD_Right,
    WD_Left,
    WD_Up,
    WD_Down,
} WallDirection;

double RayCaster_castRay(Map *map, PlayerState *player, double direction, double *x, double *y);
WallDirection getWallDirection(Map *map, PlayerState *player, double x, double y);
inline double doubleMod(double x, int y);

#endif // RAYCASTER_H
