#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <math.h>
#include "game.h"
#include "draw.h"

int main (int argc, char** argv)
{
    // initialize SDL video
    if (SDL_Init( SDL_INIT_VIDEO ) < 0)
    {
        printf("Unable to init SDL: %s\n", SDL_GetError());
        return 1;
    }

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);

    // create a new window
    SDL_Surface* screen = SDL_SetVideoMode(X_RESOLUTION, Y_RESOLUTION, 32,
                                           SDL_HWSURFACE|SDL_DOUBLEBUF);
    if (!screen)
    {
        printf("Unable to set video resolution: %s\n", SDL_GetError());
        return 1;
    }

    int mapArray[10][10] = {{1,1,1,1,1,1,1,1,1,1},
                            {1,0,1,0,1,1,1,0,1,1},
                            {1,0,1,0,0,0,0,0,1,1},
                            {1,0,0,0,1,1,1,0,0,1},
                            {1,0,0,0,1,1,1,1,0,1},
                            {1,0,0,0,1,1,0,0,0,1},
                            {1,0,0,0,1,1,0,0,0,1},
                            {1,0,0,0,0,0,0,0,0,1},
                            {1,0,0,0,1,1,0,0,0,1},
                            {1,1,1,1,1,1,1,1,1,1}};
    Map *map = NULL;
    createMap(&map, (int**)mapArray, 10, 10, MAP_GRID_DISTANCE, MAP_GRID_DISTANCE);
    //loadTexture("brickWall.bmp", &map->wallTexture);
    loadTexture("wall.bmp", &map->wallTexture);
    //loadTexture("bwhighres.bmp", &map->wallTexture);
    //loadTexture("Temple.bmp", &map->wallTexture);
    //memset(map->map, 1, sizeof(map->map));

    PlayerState ps;
    ps.x = 150;
    ps.y = 150;
    ps.direction = 0;

    // program main loop
    bool done = false;
    while (!done)
    {
        // message processing loop
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            // check for messages
            switch (event.type)
            {
                // exit if the window is closed
            case SDL_QUIT:
                done = true;
                break;

                // check for keypresses
            case SDL_KEYDOWN:
                {
                    // exit if ESCAPE is pressed
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        done = true;
                    break;
                }
            } // end switch
        } // end of message processing
        handleKeyboardInput(map, &ps);

        // DRAWING STARTS HERE

        clearScreen(screen);

        drawMap3D(map, &ps, &map->wallTexture, screen);
        drawMap2D(map, screen);
        drawPlayer2D(map, &ps, screen);
        drawRay2D(map, &ps, screen);
        // DRAWING ENDS HERE

        // finally, update the screen :)
        SDL_Flip(screen);
    } // end main loop

    // all is well ;)
    printf("Exited cleanly\n");
    return 0;
}
