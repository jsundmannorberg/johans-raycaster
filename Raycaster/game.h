#ifndef GAME_H
#define GAME_H

#include <SDL/SDL.h>
#include "map.h"

typedef struct {
    double x;
    double y;
    double direction;
} PlayerState;

typedef struct {
    PlayerState *player;
    Map *map;
    SDL_Surface *screen;
} GameState;

void handleKeyboardInput();
#endif // GAME_H
