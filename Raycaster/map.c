#include "map.h"
#include <stdlib.h>

void createMap(Map **map, int **mapArray, int xSize, int ySize, int xStart, int yStart) {
    int y;
    int x;
    Map *tmp = (Map*)malloc(sizeof(Map));
    tmp->xSize = xSize;
    tmp->ySize = ySize;
    tmp->xStart = xStart;
    tmp->yStart = yStart;
    tmp->map = (int**)malloc(ySize*sizeof(int*));
    for (y = 0; y < ySize; y++) {
        tmp->map[y] = (int*)malloc(xSize*sizeof(int));
        for (x = 0; x < xSize; x++) {
            //mapArray[y][x] is impossible since the dimensions of mapArray can't be known at compile time
            tmp->map[y][x] = *(mapArray + ySize*y + x);
        }
    }
    *map = tmp;
}

void destroyMap(Map *map) {
    int y;
    for (y = 0; y < map->ySize; y++) {
        free(map->map[y]);
    }
    free(map);
}

